<?php
namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Experience;
use App\Entity\Training;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('MonPortfolio');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Projets', 'fab fa-internet-explorer', Project::class);
        yield MenuItem::linkToCrud('Compétence', 'fas fa-battery-three-quarters', Skill::class);
        yield MenuItem::linkToCrud('Expérience', 'fas fa-award', Experience::class);
        yield MenuItem::linkToCrud('Formation', 'fas fa-file-alt', Training::class);
    }
}
