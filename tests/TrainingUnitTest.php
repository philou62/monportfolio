<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Training;

class TrainingUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $training = new Training();

        $training->setTitle('Développeur web et web mobile')
                    ->setDescription('Développer et mettre en ligne une application simple')
                    ->setCenter('Afpa Lievin');

        $this->assertTrue($training->getTitle() === 'Développeur web et web mobile');
        $this->assertTrue($training->getDescription() === 'Développer et mettre en ligne une application simple');
        $this->assertTrue($training->getCenter() === 'Afpa Lievin');
    }

    public function testIsFalse()
    {
        $training = new Training();

        $training->setTitle('Développeur web et web mobile')
                    ->setDescription('site montrant les créations d\'émaillge sur cadre carbonne')
                    ->setCenter('Afpa Lievin');

        $this->assertFalse($training->getTitle() === 'false');
        $this->assertFalse($training->getDescription() === 'false');
        $this->assertFalse($training->getCenter() === 'false');
    }


    public function testIsEmpty()
    {
        $training = new Training();

        $this->assertEmpty($training->getTitle());
        $this->assertEmpty($training->getDescription());
        $this->assertEmpty($training->getCenter());
    }
}
