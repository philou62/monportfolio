<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Project;

class ProjectUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $project = new Project();

        $project->setTitle('Cycle sobol')
                    ->setDescription('site montrant les créations d\'émaillge sur cadre carbonne')
                    ->setPicture('image/home');

        $this->assertTrue($project->getTitle() === 'Cycle sobol');
        $this->assertTrue($project->getDescription() === 'site montrant les créations d\'émaillge sur cadre carbonne');
        $this->assertTrue($project->getPicture() === 'image/home');
    }

    public function testIsFalse()
    {
        $project = new Project();

        $project->setTitle('Cycle sobol')
                    ->setDescription('site montrant les créations d\'émaillge sur cadre carbonne')
                    ->setPicture('image/home');

        $this->assertFalse($project->getTitle() === 'false');
        $this->assertFalse($project->getDescription() === 'false');
        $this->assertFalse($project->getPicture() === 'false');
    }

    public function testIsEmpty()
    {
        $project = new Project();

        $this->assertEmpty($project->getTitle());
        $this->assertEmpty($project->getDescription());
        $this->assertEmpty($project->getPicture());
    }
}
