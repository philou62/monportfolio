<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Experience;

class ExperienceUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $experience = new Experience();

        $experience->setName('Développeur Symfony')
                    ->setDescription('experience de l experience en entreprise')
                    ->setCompany('Simplon');

        $this->assertTrue($experience->getName() === 'Développeur Symfony');
        $this->assertTrue($experience->getDescription() === 'experience de l experience en entreprise');
        $this->assertTrue($experience->getCompany() === 'Simplon');
    }

    public function testIsFalse()
    {
        $experience = new Experience();

        $experience->setName('Développeur Symfony')
                    ->setDescription('experience de l experience en entreprise')
                    ->setCompany('Simplon');

        $this->assertFalse($experience->getName() === 'false');
        $this->assertFalse($experience->getDescription() === 'false');
        $this->assertFalse($experience->getCompany() === 'false');
    }

    public function testIsEmpty()
    {
        $experience = new Experience();

        $this->assertEmpty($experience->getName());
        $this->assertEmpty($experience->getDescription());
        $this->assertEmpty($experience->getCompany());
    }
}
