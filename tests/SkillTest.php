<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Skill;

class SkillTest extends TestCase
{
    public function testIsTrue()
    {
        $skill = new Skill();

        $skill->setTitle('Symfony')
                    ->setPicture('image/home');

        $this->assertTrue($skill->getTitle() === 'Symfony');
        $this->assertTrue($skill->getPicture() === 'image/home');
    }

    public function testIsFalse()
    {
        $skill = new Skill();

        $skill->setTitle('Symfony')
                    ->setPicture('image/home');

        $this->assertFalse($skill->getTitle() === 'false');
        $this->assertFalse($skill->getPicture() === 'false');
    }

    public function testIsEmpty()
    {
        $skill = new Skill();

        $this->assertEmpty($skill->getTitle());
        $this->assertEmpty($skill->getPicture());
    }
}
