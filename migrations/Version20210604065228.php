<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210604065228 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE projects_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE project (id INT NOT NULL, title VARCHAR(100) NOT NULL, description TEXT NOT NULL, picture VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE projects');
        $this->addSql('ALTER TABLE experience ALTER name TYPE VARCHAR(100)');
        $this->addSql('ALTER TABLE skill ALTER picture TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE training RENAME COLUMN end_at TO ended_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE project_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE projects_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE projects (id INT NOT NULL, title VARCHAR(100) NOT NULL, description TEXT NOT NULL, picture VARCHAR(100) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE project');
        $this->addSql('ALTER TABLE experience ALTER name TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE skill ALTER picture TYPE VARCHAR(100)');
        $this->addSql('ALTER TABLE training RENAME COLUMN ended_at TO end_at');
    }
}
